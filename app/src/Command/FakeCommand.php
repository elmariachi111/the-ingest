<?php


namespace App\Command;


use App\Entity\Offer;
use App\Entity\Product;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Generator;
use Faker\ORM\Doctrine\Populator;
use Keboola\Csv\CsvWriter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FakeCommand extends Command {

    /** @var Generator $generator */
    private $generator;

    /**
     * @var string
     */
    private $rootDir;

    public function __construct(Generator $generator, ParameterBagInterface $params) {
        $this->generator = $generator;
        $this->rootDir = $params->get('kernel.project_dir');
        parent::__construct();
    }

    protected function configure() {
        $this
            ->setName('app:fake')
            ->setDescription('Creates fake data')
            ->addArgument("count", InputArgument::OPTIONAL, "", 1000)
            ->addOption("type", null, InputOption::VALUE_OPTIONAL, "seed or update", "update")
            ->addOption("variant", "vr", InputOption::VALUE_REQUIRED, "update variant seed increment", 0)
            ->addOption("truncate", "t", InputOption::VALUE_NONE, "truncate before faking", null)
            ->setHelp('This command allows you to create fake data')
        ;
    }

    protected function execute( InputInterface $input, OutputInterface $output ) {
        if ($input->getOption("truncate")) {
            $this->truncate();
        }

        $count = $input->getArgument("count");
        $variant = $input->getOption("variant");

        $type = $input->getOption('type');
        $this->generator->seed(110178);

        $productsFile = new CsvWriter("{$this->rootDir}/products_{$type}.csv");
        $offersFile = new CsvWriter("{$this->rootDir}/offers_{$type}.csv");

        $progress = new ProgressBar($output, $count);
        $progress->setRedrawFrequency(100);
        $progress->start();

        $this->header($productsFile, $offersFile);
        if ("seed" == $type)
            $this->seed($count, $productsFile, $offersFile, $progress);
        else
            $this->update($count, $variant, $productsFile, $offersFile, $progress);
    }

    private function header(CsvWriter $productsFile, CsvWriter $offerFile) {
        $productsFile->writeRow([
            'sku', 'title', 'description'
        ]);

        $offerFile->writeRow([
            'product_sku', 'price', 'validity'
        ]);
    }

    private function update(int $count, $variant = 0, CsvWriter $productsFile, CsvWriter $offerFile, ProgressBar $progressBar) {
        $updateGen = \Faker\Factory::create();
        $updateGen->seed(377328 + $variant);
        for ($i = 0; $i < $count; $i++) {
            $product = [
                $this->generator->ean13
            ];


            if ($updateGen->randomDigit > 5)
                continue;

            $product[] = $updateGen->sentence(4);
            $product[] = $updateGen->paragraph(3);
            $productsFile->writeRow($product);
        }
    }

    private function seed(int $count, CsvWriter $productsFile, CsvWriter $offerFile, ProgressBar $progressBar) {

        for($i = 0; $i < $count; $i++) {
            $product = [
                $this->generator->ean13,
                $this->generator->sentence(4),
                $this->generator->paragraph(3)
            ];

            $productsFile->writeRow($product);
            for ($j = 0; $j < $this->generator->numberBetween(1,3); $j++) {
                $validFrom = $this->generator->dateTimeBetween("-2 years", "+2 months");
                $validityDays = $this->generator->numberBetween(4, 72);

                $validUntil = clone $validFrom;
                $validUntil->add(new \DateInterval("P{$validityDays}D"));
                $validity = $validFrom->format("Y/m/d") . "-" . $validUntil->format("Y/m/d");

                $price = $this->generator->randomFloat(2, 10, 1400);
                $offer = [
                    $product[0],
                    $price,
                    $validity
                ];
                $offerFile->writeRow($offer);
            }
            $progressBar->advance();
        }
    }

    private function truncate() {
        $connection = $this->em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->beginTransaction();
        $productTableName = $this->em->getClassMetadata(Product::class)->getTableName();
        $offerTableName = $this->em->getClassMetadata(Offer::class)->getTableName();
        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $connection->executeUpdate($dbPlatform->getTruncateTableSql($productTableName));
            $connection->executeUpdate($dbPlatform->getTruncateTableSql($offerTableName));
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
        }
        catch (\Exception $e) {
            $connection->rollback();
        }
    }
}